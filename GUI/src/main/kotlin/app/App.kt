package podkatch.gui.app

import tornadofx.*

import kodkatch.gui.views.MainView

class MainApp: App(MainView::class)
